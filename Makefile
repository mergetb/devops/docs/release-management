SHELL = /bin/bash

WORKING_DIR = $(shell pwd)

CONTAINER_NAME = registry.gitlab.com/mergetb/devops/baof/containers/asciidoctor:v2020.07.01.2

ASCIIDOCTOR_CLI_ARGS = --verbose --trace
ASCIIDOCTOR_CLI_ARGS += -b html5 -n
ASCIIDOCTOR_CLI_ARGS += -r asciidoctor-diagram
ASCIIDOCTOR_CLI_ARGS += -B . -R src -D build
ASCIIDOCTOR_CLI_ARGS += -o index.html
ASCIIDOCTOR_CLI_ARGS += 'src/site/index.adoc'

COMMON_DOCKER_RUN_ARGS = --init -t
COMMON_DOCKER_RUN_ARGS += -v "$(WORKING_DIR)/src:/documents/src"
COMMON_DOCKER_RUN_ARGS += -v "$(WORKING_DIR)/build:/documents/build"
COMMON_DOCKER_RUN_ARGS += --rm

GOSU_DOCKER_RUN_ARGS = -e GOSU_UID=$(shell id -u)
GOSU_DOCKER_RUN_ARGS += -e GOSU_GID=$(shell id -g)

.DEFAULT: all
.PHONY: all
all: clean build

.PHONY: clean
clean:
	-rm -r build/*

.PHONY: distclean
distclean: clean

.PHONY: build_images
build_images:
	-mkdir -p build/site/
	cp -v src/site/images/*.* build/site/
	# No per-component images yet, but leaving this here as an example if that arises
	#cp -v src/components/main/images/*.* build/site/

.PHONY: build
.ONESHELL:
build:
	docker run \
		$(COMMON_DOCKER_RUN_ARGS) \
		$(GOSU_DOCKER_RUN_ARGS) \
		$(CONTAINER_NAME) /bin/bash -c 'cd /documents && asciidoctor $(ASCIIDOCTOR_CLI_ARGS)'
	$(MAKE) build_images

.PHONY: ci_cd_build
.ONESHELL:
ci_cd_build:
	docker run \
		$(COMMON_DOCKER_RUN_ARGS) \
		$(CONTAINER_NAME) /bin/bash -c 'cd /documents && asciidoctor $(ASCIIDOCTOR_CLI_ARGS)'
	$(MAKE) build_images

.PHONY: debug
.ONESHELL:
debug:
	docker run \
		$(COMMON_DOCKER_RUN_ARGS) \
		$(GOSU_DOCKER_RUN_ARGS) \
		-i \
		$(CONTAINER_NAME) /bin/bash

.PHONY: preview_site
preview_site:
	docker run --init -p 8080:8080 -v $(shell pwd)/build/site:/app bitnami/nginx:1.19
