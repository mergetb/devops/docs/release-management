[#mtb_os_images_repositories]
= Repositories

.TODO
****
Given there don't seem to be many off the shelf FOSS or COTS "os image repositories" out there, we may
have to put together some mvp solution for this.

Either way, this section should talk about the way that image files get published to consumers via some
kind of "repository" like mechanism.
****