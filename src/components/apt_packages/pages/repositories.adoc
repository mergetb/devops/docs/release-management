[#mtb_apt_pkgs_repositories]
= Repositories

== Overview

The MergeTB APT repositories will be structured to align with the multiverse of releases, as well as with destination OSes.

* Each destination OS variant will get its own MergeTB APT repository hosted at `https://deb.mergetb.net/`.
** Note that for the first stable release of MergeTB, the only supported destination OS will be Debian `bullseye`
* Each of these APT repositories will contain all the MergeTB releases compatible with that destination OS.
* Each MergeTB release within one of these repositories will be grouped as one or more Debian "distributions", and named by their release code-names.
** For example, using `daruk` for the APT distribution name, not `testing`.
** In addition, all releases except for `unstable` and `experimental` will have updates and security distributions in addition to their "full" distributions.
*** For example, there will actually be 3 APT distributions for `daruk` in each repository:
**** `daruk`
**** `daruk-updates`
**** `daruk-security`

== Distributions

The APT distribution names within each APT repository will map to the overall release "universes", with qualifiers for
full, update, or security release channels:

[cols="25m,25m,25m,25m", options="header"]
|===
| APT Dist Name
| MergeTB Release Code-Name
| MergeTB Release Universe
| MergeTB Release Channel

| anju
| anju
| unstable
| full

| bellum
| bellum
| oldstable
| full

| bellum-updates
| bellum
| oldstable
| updates

| bellum-security
| bellum
| oldstable
| security

| carlov
| carlov
| stable
| full

| carlov-updates
| carlov
| stable
| updates

| carlov-security
| carlov
| stable
| security

| daruk
| daruk
| testing
| full

| daruk-updates
| daruk
| testing
| updates

| daruk-security
| daruk
| testing
| security
|===

== APT Client Configuration

=== Repository GPG keys

[cols="30d,70d", options="header"]
|===
| Destination OS
| URL for GPG key download

| Debian `bullseye`
| `https://deb.mergetb.net/keys/bullseye/mergetb_repo.gpg.public.key`
|===

=== Sources lists

Custom sources.list style configurations must be setup in APT's sources.list.d directory.

==== For Debian Bullseye

General pattern is as follows:

```
deb https://deb.mergetb.net/repos/bullseye <code_name> main
deb-src https://deb.mergetb.net/repos/bullseye <code_name> main

deb https://deb.mergetb.net/repos/bullseye <code_name>-updates main
deb-src https://deb.mergetb.net/repos/bullseye <code_name>-updates main

deb https://deb.mergetb.net/repos/bullseye <code_name>-security main
deb-src https://deb.mergetb.net/repos/bullseye <code_name>-security main
```

Where:

* `<code_name>` is the MergeTB release code-name
** For example, `daruk`.

Caveats

* The `anju` (unstable) releases do not have `-updates` or `-security` distributions.

.Example for the `anju` release
```
deb https://deb.mergetb.net/repos/bullseye anju main
deb-src https://deb.mergetb.net/repos/bullseye anju main
```

.Example for the `daruk` release
```
deb https://deb.mergetb.net/repos/bullseye daruk main
deb-src https://deb.mergetb.net/repos/bullseye daruk main

deb https://deb.mergetb.net/repos/bullseye daruk-updates main
deb-src https://deb.mergetb.net/repos/bullseye daruk-updates main

deb https://deb.mergetb.net/repos/bullseye daruk-security main
deb-src https://deb.mergetb.net/repos/bullseye daruk-security main
```
