[#mtb_ecosystem_governance]
= Governance

The section is intended to document the social governance of the MergeTB release ecosystem.

It will only describe the minimal-viable organizational approach we have at the moment.

== Roles and Responsibilities

Folks participating in the release ecosystem will likely work in one or many roles at a time due to team
size and other constraints.  But while wearing each "hat" they should endeavor to follow that hat's
priorities in good faith.


[cols="20d,80a", options="header"]
|===
| Role
| Responsibilities

| Source Project Maintainer
| Maintain one or more source projects and take responsibility for key choices in their lifecycles at a local level

| Release Engineer
| Maintain the release management procedures, tools, and infrastructure.  Take responsibility for key choices in the global
  lifecycle of the release ecosystem.

| Software Test Engineer
| Plan, design, and execute software testing for the ecosystem as a whole.
|===

=== Source Project Maintainers

Things you are responsible for:

* Reviewing and approving/disapproving merge requests to your source project(s)
* Local software testing of your source project(s)
** Include unit testing as well as any integration testing or other concerns that don't span the entire ecosystem
* Local packaging of the deliverables from your source project(s)
** You are responsible for building the deliverable (whether its a deb package, container image, or os image), which includes
   versioning it's packaging correctly (according to the deliverable's species specific rules as well as the general rules of
   the release ecosystem).
** The rest of the ecosystem (and release engineers) will manage how that deliverable gets released to a merge universe,
   and how that merge universe gets distributed to its consumers
* Mentoring release engineers and software test engineers in the particulars of your deliverables in regards to release and
  testing

=== Release Engineers

Things you are responsible for:

* Warehousing of the deliverables from source project(s) while they are waiting for inclusion in release universes
* Release of the warehoused deliverables into release universes via release channels
* Distribution of release universes to consumers
* Implementation and maintenance of release management procedures, tools, and infrastructure
* Mentoring source project maintainers and software test engineers in the particulars of the release management world

=== Software Test Engineers

Things you are responsible for:

* Planning, designing, and execution of ecosystem scoped test agendas
** Should be supplemented by source project maintainers' local testing efforts
* Validating deliverables against any product specifications or ecosystem-level criteria
* Implementation and maintenance of ecosystem scoped test procedures, tools, and infrastructure
* Mentoring source project maintainers in their efforts to test and validate their projects at a local scope